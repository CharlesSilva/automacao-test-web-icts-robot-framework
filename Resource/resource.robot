*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${URL}             https://www.amazon.com.br/
${BROWSER}         chrome
${CLICK_BTN}       css=div[id="nav-xshop-container"] a[data-csa-c-content-id="nav_cs_bestsellers"]
${CLICK_PRODUTO}   css=div [class="a-carousel-card"] img[alt="Echo Dot (3ª Geração): Smart Speaker com Alexa - Cor Preta"]
${CLICK_CARRINHO}  css=*[id="add-to-cart-button"]
${CLICK_FECHAR}    xpath=//*[@class="a-popover-wrapper"]//*[@aria-label="Close"]
${BTN_CARRINHO}    css=a [id="nav-cart-count-container"] 

*** Keywords ***
Dado que estou na pagina Amazon
    Open Browser   url=${URL}      browser=${BROWSER}
    Maximize Browser Window

Quando acesso a pagina clico em "Mais vendidos" faço a procura e seleciono clicando no produto "Echo Dot"
    Click Link                     ${CLICK_BTN}
    Wait Until Element Is Visible  ${CLICK_PRODUTO}
    Click Element                  ${CLICK_PRODUTO}
    Sleep                          2s

Então clico no botão em adicionar no carrinho fecho a janela com sugestões de produtos e verifico se a mensagem "${MSG_DESEJADA}" apareceu
    Click Element                  ${CLICK_CARRINHO}
    Wait Until Element Is Visible  ${CLICK_FECHAR}
    Click button                   ${CLICK_FECHAR}
    Page Should Contain            ${MSG_DESEJADA}

E então vou ao carrinho clico no seu ícone e verifico se ha um "${MSG_DESEJADA}"
    Click Element                  ${BTN_CARRINHO}
    Page Should Contain            ${MSG_DESEJADA}
    Sleep                          2s
    Capture Page Screenshot
    Close Browser