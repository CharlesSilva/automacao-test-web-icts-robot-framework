*** Settings ***
Resource  ../Resource/resource.robot

*** Test Cases ***
Caso de teste 01: Adicionar produto no carrinho
    Dado que estou na pagina Amazon
    Quando acesso a pagina clico em "Mais vendidos" faço a procura e seleciono clicando no produto "Echo Dot"
    Então clico no botão em adicionar no carrinho fecho a janela com sugestões de produtos e verifico se a mensagem "Adicionado ao carrinho" apareceu
    E então vou ao carrinho clico no seu ícone e verifico se ha um "Subtotal (1 item)"

